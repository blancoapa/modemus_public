# Modelling nanoparticle transport in the extracellular matrix: improving drug delivery with ultrasounds (ModEMUS)

In this repository, we share the current stable version of the code used for ModEMUS for public use under the GNU GENERAL PUBLIC LICENSE. 
Public use and distribution of the code is permited, under the proper acknowledgment to its authors.
Published results within ModEMUS, as well as other outputs produced within this project will be made available in this repository.

## Objective

The overall objective of ModEMUS is to correlate the molecular details of nanoparticles (NPs) and the extracellular matrix (ECM), and the effect of focused ultrasound (FUS) exposure with NP transport across the ECM.

## Authors and acknowledgment

### Principal investigators

- Pablo M. Blanco
- Catharina de Lange Davies
- Rita S. Dias

### Master students
- Charlotte Borge Hunskår
- Marit Svendby Otervik

### Collaborators

- Jean-Noël Grad
- Peter Košovan
- Christian Holm


### Acknowledgement
All the members of the repository acknowledge the funding provided by the European Union’s Horizon Europe research and innovation program under the Marie Skłodowska-Curie grant agreement No 101062456 (ModEMUS).
