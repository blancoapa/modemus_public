


"""
Simulates the diffusion of spherical nanoparticles through a diamond-structured polymer network.
All the parameters and set-up of the system has been designed to mimic the experiments done in:

Einen, C.; Price, S.E.N.; Ulvik, K.; Gjennestad, M.A.; Hansen, R.; Kjelstrup, S.; Davies, C.d.L. Nanoparticle Dynamics in Composite Hydrogels Exposed to Low-Frequency Focused Ultrasound. Gels 2023, 9, 771. https://doi.org/10.3390/gels9100771
"""

import numpy as np
import os
import inspect
import pint
import sys
import argparse
import time
import pandas as pd

import espressomd
import espressomd.polymer
import espressomd.io.writer.vtf  # pylint: disable=import-error
import espressomd.accumulators
import espressomd.observables
from espressomd import checkpointing


current_dir= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
path_end_index=current_dir.find("modemus")
root_dir = current_dir[0:path_end_index]+"modemus"
sys.path.insert(0, root_dir)

from pmb_handy_tools.lib import analysis
from pmb_handy_tools.lib import misc_tools as misc

code_path= root_dir + "/code"
sys.path.insert(0, code_path)

structure_folder = root_dir + "/hydrogel_structures"

from lib import hydrogel
from lib import analysis as an
from lib import np_tools

espressomd.assert_features(["WCA","THERMOSTAT_PER_PARTICLE"])


# Inputs from terminal
parser = argparse.ArgumentParser(description='Simulate transport of NPs through an agarose hydrogel')
## System inputs
parser.add_argument('--vstream', type=float, default=0,  help='accoustic streaming velocity in m/s')
parser.add_argument('--medium', type=str, default="water",  help='medium of the simulation, it can be either water or agarose')
parser.add_argument('--initialization', type=str, default = "random", help='nanoparticle initialization type, "random" or "yz-lattice"')
parser.add_argument('--structurefile', type=str, default=None, help="The structurefile to load, if agarose, in modemus/hydrogel_structures/")
parser.add_argument('--boxl', type=float, default=100, help="When in water, can adjust boxl manually. In reduced length")
## Cluster-related inputs
parser.add_argument('--rank', type=int, default=0,  help='id of the core executing the code, used to set the SEED for the random number generator')
parser.add_argument('--reqtime', type=str, default=None, help='Time requested in the cluster, format HH:MM:SS')
parser.add_argument('--restart', action='store_true')
parser.add_argument('--no-restart', dest='restart', action='store_false')
parser.set_defaults(restart=False)
args = parser.parse_args ()

# Sanity checks
initialization_type = args.initialization
supported_initialization_modes=["random","yz-lattice"]
if initialization_type not in supported_initialization_modes:
    raise ValueError(f"initialization type {initialization_type} not supported, supported modes are {supported_initialization_modes}")

medium = args.medium
valid_media=["water", "agarose"]
if medium not in valid_media:
    raise ValueError(f"The medium {medium} is not supported, supported media are {valid_media}")

structurefile = args.structurefile
if medium == "agarose": 
    if structurefile is None:
        raise ValueError("A structurefile is not loaded")
    elif "relaxed" not in structurefile:
        raise ValueError("You have loaded a non-relaxed structure. Please load a structure ending with '_relaxed'")
    structurefile_wo_r = structurefile[:-8]
else:
    if args.boxl <= 0 or None: 
        raise ValueError(f"The provided boxl {args.boxl} is not supported. In medium = water, a boxl > 0 is needed")
    if structurefile is not None and medium != "agarose":
        raise ValueError("The medium is not agarose, but a structurefile is loaded. Please either change the medium to agarose, or if in water, don't load a structurefile")

# Setup the units of the system
units=pint.UnitRegistry()
Kb=1.38064852e-23 * units.J / units.K
N_A=6.02214076e23 / units.mol
temperature=298.15 * units.K
kT=Kb*temperature

SEED=42+args.rank

unit_length=10*units.nanometer # Based on Bertula et al. 2019 and Martikainen et al. 2020
unit_time=100*units.nanosecond
unit_energy=Kb*temperature
unit_mass=unit_energy*unit_time**2/unit_length**2

units.define(f'reduced_length = {unit_length}')
units.define(f'reduced_time = {unit_time}')
units.define(f'reduced_mass = {unit_mass}')
units.define(f'reduced_energy = {unit_energy}')

sigma=units.Quantity(1, "reduced_length")

print("The reduced unit system is ")
print(f'reduced_length = {unit_length.to("nm")}')
print(f'reduced_time =   {unit_time.to("ns")}')
print(f'reduced_energy = {unit_energy.to("J")}')
print(f'reduced_mass =   {unit_mass.to("kg")} = {(unit_mass*N_A).to("kg / mol")}')

dt=units.Quantity(0.001, "reduced_time")
print(f"the time step is {dt.to('ns')}")

computing_time=misc.parse_computing_time(reqtime=args.reqtime,
                                         units=units)
start_time=units.Quantity(time.time(),"s")

# Set-up the checkpoint
checkpoint = checkpointing.Checkpoint(checkpoint_id=f"mycheckpoint_rank{args.rank}", checkpoint_path=".")
checkpoint_structure = checkpointing.Checkpoint(checkpoint_id=f"{args.structurefile}", checkpoint_path=structure_folder)
np.random.seed(seed=SEED)


# System parameters
#############################################################
if not args.restart and medium=="agarose":
    checkpoint_structure.load()
    checkpoint.register("system") 
    checkpoint.register("hydrogel_map")
    print(f"Loading structure {args.structurefile}")
    system.integrator.run(2, reuse_forces=True)
    

    
elif not args.restart and medium=="water":
    n_copies = 0
    system = espressomd.System(box_l=[10, 10, 10])
    system.time_step = dt.to("reduced_time").magnitude
    system.cell_system.skin = 0.4
    checkpoint.register("system")
    
else:
    checkpoint.load() 

structure_dict = analysis.get_params_from_dir_name(structurefile_wo_r)
param_df=analysis.read_csv_file(path=f'{structure_folder}/{structurefile}/{structurefile}_params.csv')
n_copies = int(structure_dict["ncopies"])

# Simulation parameters
N_cycles = 10000
MD_steps = 100
delta_N = 100
N_snapshots = 100 # Number of snapshots of the system for visualization
delta_histogram = 1000 # Numer of cycles in simulation bfore histogram is written to file and dropped
equilibration_time=units.Quantity(1000,"ns") 
pore_size_distribution = False #Controls if calculating the pore size distribution later
if N_snapshots > N_cycles:
    raise ValueError(f"The number of snapshots {N_snapshots} has to be smaller or equal to the number of simulation cycles {N_cycles}.")
 
eta=units.Quantity(0.89, "mPa s")           # for water at 298.15
epsilon=1
lj=True


if medium == "agarose":
    polymer_diameter = float(param_df._get_value(0,"agarose_bead_diameter_nm"))*units.Quantity(1, "nm")
    bond_length = float(param_df._get_value(0,"bond_length_nm"))*units.Quantity(1,"nm").to("reduced_length")

    # monomers per chain for diamond lattice
    monomers_per_fibril = int(structure_dict["mpc"])
    print(f"monomers per fibril {monomers_per_fibril}")

    types={"gel-n-nodes": 0,
       "gel-c-nodes": 1,
       "gel-n-monomers":2,
       "gel-c-monomers":3,
       "np":4, 
       "A":5}
    
    charges = {
    types["gel-n-nodes"]: 0,
       types["gel-c-nodes"]: 0,
       types["gel-n-monomers"]:0,
       types["gel-c-monomers"]:0,
       types["np"]:0,
       types["A"]:0}
else: 
    types={
       "np":4}
    
    charges = {
       types["np"]:0}
    
    
nanoparticle_diameter=units.Quantity(87,"nm") 
nanoparticle_density=units.Quantity(1.055, "g/cm**3")
nanoparticle_concentration=units.Quantity(5, "microgram/mL")

print(f"The diameter of the nanoparticles is {nanoparticle_diameter.to('nm')}")

diameters={}

for key in types.keys():
    if key == "np":
        diameters[types[key]]=nanoparticle_diameter.to("reduced_length")
    else: 
        if medium == "agarose":
            diameters[types[key]]=polymer_diameter.to("reduced_length")

print(f"the total simulation time is {(N_cycles*MD_steps*dt).to('microseconds')}")


if medium == "agarose":
    box_l = system.box_l[1]*units.Quantity(1, "reduced_length") #as before, using mpc to create box_l, even if in water
else:
    box_l = args.boxl*units.Quantity(1, "reduced_length")
box_l_long=box_l*(n_copies+1)
volume=box_l**2*box_l_long

print(f"The size of the simulation box is {box_l.to('reduced_length')} x {box_l.to('reduced_length')} {box_l_long.to('reduced_length')}")
print(f"equivalent to  {box_l.to('nm')} x {box_l.to('nm')} x {box_l_long.to('nm')}")


mass_np=4./3.*np.pi*(nanoparticle_diameter/2)**3*nanoparticle_density

print(f"The mass of each nanoparticle is {mass_np.to('reduced_mass')} = {(mass_np*N_A).to('kg / mol')}")

gamma_nanoparticles=6*np.pi*eta*nanoparticle_diameter/(2*unit_mass)
D_np=kT/(gamma_nanoparticles*unit_mass)


print(f"gamma nanoparticles {gamma_nanoparticles.to('1/ns')} reduced_gamma = {gamma_nanoparticles.to('1/reduced_time')}")
print(f"D(NP) = {D_np.to('nm**2/ns')}")

# Add an external force
vstream = units.Quantity(args.vstream, "m/s")
ext_force_np = gamma_nanoparticles*unit_mass* vstream
if medium == "agarose":
    ext_force_polymer_beads = float(param_df._get_value(0,"agarose_gamma_ns"))*units.Quantity(1,"1/ns")*unit_mass* vstream


particle_properties_dict = {
    "np_type": types["np"],
    "np_diameter": nanoparticle_diameter,
    "polymer_bead_diameter": units.Quantity(0,"nm"),
    "np_concentration" : nanoparticle_concentration,
    "np_gamma": [gamma_nanoparticles.to("1/reduced_time").magnitude]*3,
    "np_mass": mass_np,
    "box_copies": n_copies,
    "box_volume" : volume
}
if medium == "agarose":
    particle_properties_dict["polymer_bead_diameter"] = polymer_diameter

if args.restart:
    mode="a"
    histogram_mode = "a"
else:
    mode="w"
    histogram_mode = "w"
outfile = open('hydrogel_simulation.vtf', mode=mode)

if not args.restart:
    if medium == "water":
        system.box_l = [box_l_long.to("reduced_length").magnitude, box_l.to("reduced_length").magnitude,box_l.to("reduced_length").magnitude]

    # Setup the interactions in the system
    if lj:
        cutoff=2**(1./6.)*units('reduced_length')
        for p_type in types.values(): #only adding interactions between the np and polymer, polymer-polymer loaded through checkpoint
            diameter_sum=diameters[types["np"]]+diameters[p_type]
            offset=diameter_sum/2.-sigma
            pair_cutoff=cutoff  
            system.non_bonded_inter[types["np"],p_type].lennard_jones.set_params(epsilon = epsilon,
                                                                                        sigma   = sigma.to('reduced_length').magnitude,
                                                                                        cutoff  = pair_cutoff.to('reduced_length').magnitude,
                                                                                        offset  = offset.to('reduced_length').magnitude,
                                                                                        shift   = "auto",
                                                                                        )

    
    # Adding NPs to the system
    np_tools.add_NPs_to_system(system=system,
                                particle_properties_dict=particle_properties_dict,
                                initialization_type = initialization_type)

    number_of_NPs = len(system.part.select(type=types["np"]))
    if initialization_type == "random":
        print("the number of nanoparticles in the simulation box is:", number_of_NPs)
        c_np_calc=number_of_NPs/volume.to("mL")*mass_np.to("g")
        print(f"The concentration of nanoparticles in the simulation box is {c_np_calc.to('gram/L')}")

    elif initialization_type == "yz-lattice": 
        print("Number of NPs placed in lattice:",number_of_NPs)
        c_np_calc = units.Quantity(np.nan, "microgram/mL")

    checkpoint.register("number_of_NPs")
    checkpoint.register("c_np_calc")
    
    if vstream != 0:
        for label in types:
            # NOTE: Assuming that only NPs and hydrogel particles are in the simulation box
            if label == "np":
                system.part.select(type=types[label]).ext_force= [ext_force_np.to("reduced_mass*reduced_length/reduced_time**2").magnitude,0,0]
            else:
                if medium == "agarose":
                    if system.part.select(type=types[label]):
                        system.part.select(type=types[label]).ext_force= [ext_force_polymer_beads.to("reduced_mass*reduced_length/reduced_time**2").magnitude,0,0]   

    if medium == "water":
        ############################################################
        #     Warmup                                            #
        ############################################################
        system.thermostat.turn_off()
        # minimize energy using min_dist as the convergence criterion
        system.integrator.set_steepest_descent(f_max=0, 
                                            gamma=1,
                                            max_displacement=0.001)
        system.integrator.run(10000)

        print(f"minimization: {system.analysis.energy()['total']:+.2e}")
        print()
        system.integrator.set_vv()

        # activate thermostat
        system.thermostat.set_langevin(kT=kT.to("reduced_energy").magnitude, 
                                    gamma=1, 
                                    seed=SEED)


# visualize after minimization
espressomd.io.writer.vtf.writevsf(system, outfile)
espressomd.io.writer.vtf.writevcf(system, outfile)

# Setup the sampling of the mean square displacement and the velocity

pos_obs = espressomd.observables.ParticlePositions(ids=system.part.select(type=types["np"]).id)
v_obs = espressomd.observables.ParticleVelocities(ids=system.part.select(type=types["np"]).id)
c_v=espressomd.accumulators.TimeSeries(obs=v_obs,
                                       delta_N=delta_N)
system.auto_update_accumulators.add(c_v)

input_dict={
            "vstream": args.vstream,
            "medium": medium,
            "initialization": args.initialization,
            "ncopies": n_copies,
            "rank": args.rank
            }
if medium == "agarose":
    input_dict["mpc"] = monomers_per_fibril
    input_dict["dends"] = int(structure_dict["dends"])
    input_dict["dends"] = int(structure_dict["ldends"])
else:
    input_dict["boxl"] = box_l.to("nm").magnitude

output_name=misc.built_output_name(input_dict=input_dict)

# Store all parameters into a pandas dataframe

param_dict={"np_diameter_nm": [nanoparticle_diameter.to("nm").magnitude],
            "np_density_g_cm3": [nanoparticle_density.to("g/cm**3").magnitude],
            "np_number": [number_of_NPs],
            "np_concentration_mg_ml": [c_np_calc.to("microgram/mL").magnitude],
            "np_mass_kg_mol": [(mass_np*N_A).to('kg / mol').magnitude],
            "np_gamma_ns": [gamma_nanoparticles.to('1/ns').magnitude],
            "np_diff_nm_ns": [D_np.to('nm**2/ns').magnitude],
            "fus_NP_pN": [ext_force_np.to("pN").magnitude]}
if medium == "water":
    param_dict["boxl_nm"] = [box_l.to("nm").magnitude]
    param_dict["boxl_long_nm"] = [box_l_long.to("nm").magnitude]
else:
    param_dict["fus_hydrogel_pN"] = [ext_force_polymer_beads.to("pN").magnitude]
    
if args.rank == 0:
    index_rank=output_name.find("rank")
    params_out_name=output_name[:index_rank-1]+output_name[index_rank+len("rank-0")+1:]
    new_param_df = pd.DataFrame.from_dict(param_dict)
    if medium == "agarose":
        new_param_df = pd.concat([param_df, new_param_df], axis=1)
    new_param_df.to_csv(f'{params_out_name}_params.csv',index=False)

# Initialize dict to store time series of the observables

obs_dict={"time":[], 
          "persistence_length_nm" : [], 
          "end_to_end_distance_nm":[]}

if pore_size_distribution:
    pore_size_distribution_dict = {"time":[], 
                                   "pore_sizes_nm":[]}

conc_dict={}
conc_histogram_start = (-box_l_long/2).to("reduced_length").magnitude
conc_histogram_stop = (box_l_long/2).to("reduced_length").magnitude
histogram_step_size = (nanoparticle_diameter).to("reduced_length").magnitude * 1.5

initial_simulation_time=time.time()*units.s

total_simulation_time=MD_steps*N_cycles*dt
if total_simulation_time.to("ns").magnitude < equilibration_time.to("ns").magnitude:
    print(f"WARNING: Your total simulation time {total_simulation_time.to('ns')} is below the equilibration time {equilibration_time.to('ns')}")
    equilibration=False

if not args.restart:
    N_frame=1
    checkpoint.register("N_frame")
N_time=2

if args.restart:
    print("\n*** Restarting simulation ***\n")
    equilibration=False
else:
    print("\n*** Starting simulation ***\n")
    equilibration=True

if args.restart:
    system.auto_update_accumulators.add(c_pos)

for cycle in range(N_cycles):    
    system.integrator.run(MD_steps, reuse_forces=True)
    if args.rank == 0:
        if cycle % N_snapshots == 0:
            espressomd.io.writer.vtf.writevcf(system, outfile)
    
    sim_time=units.Quantity(system.time, "reduced_time")

    if sim_time.to("ns").magnitude > equilibration_time.to("ns").magnitude and equilibration:
        equilibration=False
        # Setup the sampling of the mean square displacement
        pos_obs = espressomd.observables.ParticlePositions(ids=system.part.select(type=types["np"]).id)
        c_pos = espressomd.accumulators.Correlator(
                                        obs1=pos_obs, 
                                        tau_lin=16, 
                                        tau_max=MD_steps*N_cycles, 
                                        delta_N=delta_N,  # Defines every how many dt is the observable sampled
                                        compress1="discard1",
                                        corr_operation="square_distance_componentwise")
        system.auto_update_accumulators.add(c_pos)
        checkpoint.register("c_pos")

    # Store time
    obs_dict["time"].append(system.time)
    if initialization_type == "yz-lattice":
        # Calculating concentration profile
        conc_histogram, histogram_bins = np_tools.calculate_concentration_histogram(system=system,
                                                                    start=conc_histogram_start,
                                                                    stop=conc_histogram_stop,
                                                                    step_size=histogram_step_size,
                                                                    particle_properties_dict=particle_properties_dict)
        histogram_volume = (histogram_bins[-2]-histogram_bins[-3]) * box_l**2
        conc_dict[system.time]=conc_histogram/histogram_volume.magnitude
        if cycle%delta_histogram==0 or cycle == N_cycles -1:
            conc_df = pd.DataFrame.from_dict(conc_dict,orient="index",columns=histogram_bins[:-1].astype(str))
            conc_df.index.name = "time"
            conc_df.reset_index(inplace=True)
            if histogram_mode == "w":
                conc_df.to_csv(path_or_buf=f'{output_name}_concentration_profile.csv',
                               mode="w",
                               index=False)
                histogram_mode = "a"
            elif histogram_mode == "a":
                conc_df.to_csv(path_or_buf=f'{output_name}_concentration_profile.csv',
                           mode="a",
                           header=False,
                           index=False)
            conc_dict={}
            
    # Measure and store observables 
    if medium == "agarose":
        
        end_to_end_distance_values = np.array([])
        for end_to_end_distance in hydrogel.calculate_end_to_end_distances(hydrogel_map=hydrogel_map, system=system):
            end_to_end_distance_values = np.append(end_to_end_distance_values, end_to_end_distance*units.Quantity(1, "reduced_length").to("nm").magnitude)
        obs_dict["end_to_end_distance_nm"].append(end_to_end_distance_values.mean())
        
        persistence_length_values = np.array([])
        for persistence_length in hydrogel.calculate_persistence_length_between_nodes(monomers_per_fibril=monomers_per_fibril, hydrogel_map=hydrogel_map, bond_length=bond_length, system=system):
            persistence_length_values = np.append(persistence_length_values, persistence_length*units.Quantity(1, "reduced_length").to("nm").magnitude)
        obs_dict["persistence_length_nm"].append(persistence_length_values.mean())
        if pore_size_distribution:  
            pore_size_distribution_dict["time"].append(system.time)
            pore_size_values=np.array([])
            for pore_size in hydrogel.calculate_pore_sizes(hydrogel_map=hydrogel_map, system=system):
                pore_size_values = np.append(pore_size_values, pore_size*units.Quantity(1, "reduced_length").to("nm").magnitude)
            pore_size_distribution_dict["pore_sizes_nm"].append(pore_size_values)
        
    else:
        obs_dict["persistence_length_nm"].append(np.nan)
        obs_dict["end_to_end_distance_nm"].append(np.nan)
        if pore_size_distribution:
            pore_size_distribution_dict["pore_sizes_nm"].append(np.nan)
    
    # Report progress on a logarithmic scale to avoid spam on terminal
    if (cycle == int(N_time)):
        N_time=N_time*1.5
        misc.write_progress(step=cycle, 
                            total_steps=N_cycles, 
                            initial_simulation_time=initial_simulation_time, 
                            units=units)

    # Prevent the simulation to be killed by the cluster
    current_time=units.Quantity(time.time(),"s")
    time_expended=current_time-start_time
    if computing_time:
        fraction_completed=(time_expended/computing_time).magnitude
        if fraction_completed > 0.95:
            print(f"95% of the requested computing time has been consumed, exiting promtly at cycle {cycle}")
            break

# Save the final state of the system in case we need to restart the simulation
checkpoint.save()

print("\n*** Simulation finished ***\n")

# Write outputs

binning_obs_df = pd.DataFrame.from_dict(obs_dict)
if pore_size_distribution:
    pore_size_df = pd.DataFrame.from_dict(pore_size_distribution_dict)
    pore_size_df.to_csv(f'{output_name}_pore_size.csv')

if args.restart:
    prev_obs_df=analysis.read_csv_file(path=f'{output_name}_binning_obs.csv')
    binning_obs_df=analysis.merge_time_series_dfs(list_time_series_df=[prev_obs_df,binning_obs_df])
    if pore_size_distribution:
        prev_pore_size_df=analysis.read_csv_file(path=f'{output_name}_pore_size.csv')
        pore_size_df=analysis.merge_time_series_dfs(list_time_series_df=[prev_pore_size_df,pore_size_df])
        pore_size_df.to_csv(f'{output_name}_pore_size.csv')

binning_obs_df.to_csv(f'{output_name}_binning_obs.csv')


outfile.close()
if args.rank == 0:
    misc.write_vmd_visualization_script(particle_types_list=types.values(), 
                                            filename='hydrogel_simulation.vtf',
                                            diameters=diameters)
# Write MSD

#c_pos.finalize()
average_msd=np.average(c_pos.result(), axis=1)
time=c_pos.lag_times()*units.Quantity(1,"reduced_time")
dist_axis=(average_msd.reshape([-1, 3])*units.Quantity(1,"reduced_length**2"))
r2=dist_axis[:,0]+dist_axis[:,1]+dist_axis[:,2]

msd_dict={"time_ns":time.to("ns").magnitude,
          "N_samples": c_pos.sample_sizes(),
          "r2_x_nm2":  dist_axis[:,0].to("nm**2").magnitude,
          "r2_y_nm2":  dist_axis[:,1].to("nm**2").magnitude,
          "r2_z_nm2":  dist_axis[:,2].to("nm**2").magnitude,
          "r2_nm2": r2.to("nm**2").magnitude
        }

time_series_df = pd.DataFrame.from_dict(msd_dict)
time_series_df.to_csv(f'{output_name}_time_series.csv')

# Write velocity

# Average of NPs
v_time_series=np.mean(np.array(c_v.time_series()), axis=1)
v_time_series_squared=np.mean((np.array(c_v.time_series()))**2, axis=1)

v_x=v_time_series[:,0]*units.Quantity(1,"reduced_length/reduced_time")
v_y=v_time_series[:,1]*units.Quantity(1,"reduced_length/reduced_time")
v_z=v_time_series[:,2]*units.Quantity(1,"reduced_length/reduced_time")

v_x2=v_time_series_squared[:,0]*units.Quantity(1,"reduced_length**2/reduced_time**2")
v_y2=v_time_series_squared[:,1]*units.Quantity(1,"reduced_length**2/reduced_time**2")
v_z2=v_time_series_squared[:,2]*units.Quantity(1,"reduced_length**2/reduced_time**2")

v_time=np.linspace(0,
                N_cycles*MD_steps*dt.to("reduced_time").magnitude,
                len(v_z))

v_dict={"time": v_time,
        "v_x": v_x.to("m/s").magnitude,
        "v_y": v_y.to("m/s").magnitude,
        "v_z": v_z.to("m/s").magnitude,
        "v_x2": v_x2.to("m**2/s**2").magnitude,
        "v_y2": v_y2.to("m**2/s**2").magnitude,
        "v_z2": v_z2.to("m**2/s**2").magnitude}

v_df = pd.DataFrame.from_dict(v_dict)

if args.restart:
    prev_v_df=analysis.read_csv_file(path=f'{output_name}_velocity.csv')
    v_df=analysis.merge_time_series_dfs(list_time_series_df=[prev_v_df,v_df])
   
v_df.to_csv(f'{output_name}_velocity.csv')

print("\n*** Data stored ***")


