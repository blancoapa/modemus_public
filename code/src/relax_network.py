import os
import inspect
import pint
import sys
import argparse
import time
import pandas as pd

import espressomd
import espressomd.polymer
import espressomd.io.writer.vtf  # pylint: disable=import-error
import espressomd.accumulators
import espressomd.observables
from espressomd import checkpointing


current_dir= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
path_end_index=current_dir.find("modemus")
root_dir = current_dir[0:path_end_index]+"modemus"
sys.path.insert(0, root_dir)

from pmb_handy_tools.lib import analysis
from pmb_handy_tools.lib import misc_tools as misc

code_path= root_dir + "/code"
sys.path.insert(0, code_path)

structure_folder = root_dir + "/hydrogel_structures"

from lib import hydrogel

espressomd.assert_features(["WCA","THERMOSTAT_PER_PARTICLE"])

# Inputs from terminal
parser = argparse.ArgumentParser(description='Relax hydrogel structure')
## System inputs
parser.add_argument('--structurefile', type=str, default="mpc-30_ncopies-0_dends-0_ldends-0_xlinks-False", help="The structurefile to load, in modemus/hydrogel_structures/")
parser.add_argument('--fres', type=float, default=1,  help='Rescaling factor to compress the hydrogel, number between 0 and 1')
parser.add_argument('--swellingeq', action='store_true')
parser.add_argument('--no-swellingeq', dest='swellingeq', action='store_false')
parser.set_defaults(swellingeq=False)
parser.add_argument('--restartpv', action='store_true')
parser.add_argument('--no-restartpv', dest='restartpv', action='store_false')
parser.set_defaults(restartpv=False)
## Cluster-related inputs
parser.add_argument('--rank', type=int, default=0,  help='id of the core executing the code, used to set the SEED for the random number generator')
parser.add_argument('--reqtime', type=str, default=None, help='Time requested in the cluster, format HH:MM:SS')
args = parser.parse_args ()


# Setup the units of the system
units=pint.UnitRegistry()
Kb=1.38064852e-23 * units.J / units.K
N_A=6.02214076e23 / units.mol
temperature=298.15 * units.K
kT=Kb*temperature

SEED = 42 + args.rank

unit_length=10*units.nanometer # Based on Bertula et al. 2019 and Martikainen et al. 2020
unit_time=100*units.nanosecond
unit_energy=Kb*temperature
unit_mass=unit_energy*unit_time**2/unit_length**2

units.define(f'reduced_length = {unit_length}')
units.define(f'reduced_time = {unit_time}')
units.define(f'reduced_mass = {unit_mass}')
units.define(f'reduced_energy = {unit_energy}')

sigma=units.Quantity(1, "reduced_length")

print("The reduced unit system is ")
print(f'reduced_length = {unit_length.to("nm")}')
print(f'reduced_time =   {unit_time.to("ns")}')
print(f'reduced_energy = {unit_energy.to("J")}')
print(f'reduced_mass =   {unit_mass.to("kg")} = {(unit_mass*N_A).to("kg / mol")}')

dt=units.Quantity(0.001, "reduced_time")
print(f"the time step is {dt.to('ns')}")

computing_time=misc.parse_computing_time(reqtime=args.reqtime,
                                         units=units)
start_time=units.Quantity(time.time(),"s")


# Set-up the checkpoint
checkpoint_structure = checkpointing.Checkpoint(checkpoint_id=f"{args.structurefile}", checkpoint_path=structure_folder)
checkpoint_relaxed_structure = checkpointing.Checkpoint(checkpoint_id=f"{args.structurefile}_relaxed", checkpoint_path=structure_folder)
checkpoint_PV = checkpointing.Checkpoint(checkpoint_id=f"PVcheckpoint{args.rank}", checkpoint_path=".")

# System parameters
#############################################################
if args.restartpv:
    if args.swellingeq:
        raise ValueError("You cannot restart a PV protocol when swelling eq. is already found for this structure. Swellingeq must be false")
    checkpoint_PV.load()
else:
    checkpoint_structure.load()   

param_df = analysis.read_csv_file(path=f"{structure_folder}/{args.structurefile}/{args.structurefile}_params.csv")
  
if args.swellingeq:
    checkpoint_relaxed_structure.register("system") 
    checkpoint_relaxed_structure.register("hydrogel_map")
    outfile_structure = open(f'{structure_folder}/{args.structurefile}/{args.structurefile}.vtf', mode="a")

    fres = hydrogel.get_eq_swelling(args.structurefile)
    
    print(f"Rescaling structure {args.structurefile} to the swelling equilibrium: {fres}")
    hydrogel.rescaling_cubic_box_to_desired_box_l(relative_box_l=fres, system=system, reuse_forces=True)
    
    param_to_add = {
                "fres": fres
                }
    boxl = system.box_l[1]*units.Quantity(1, "reduced_length").to("nm")
    param_df.at[0, "boxl_nm"] = boxl.magnitude
    param_to_add_df = pd.DataFrame(param_to_add, index=[0]) 
    param_df = pd.concat([param_df, param_to_add_df], axis=1)
    param_df.to_csv(f"{structure_folder}/{args.structurefile}_relaxed/{args.structurefile}_relaxed_params.csv", index=False)
    
    espressomd.io.writer.vtf.writevcf(system, outfile_structure)
    outfile_structure.close()
    checkpoint_relaxed_structure.save()
    print("Rescaling saved")

#PV protocol
else: 
    checkpoint_PV.register("system")
    outfile_PV = open(f'PVprotocol_fres-{args.fres}.vtf', mode="w")
    espressomd.io.writer.vtf.writevcf(system, outfile_PV)
    
    print(f"Rescaling structure {args.structurefile} by PV protocol to fres: {args.fres}")
    hydrogel.rescaling_cubic_box_to_desired_box_l(relative_box_l=args.fres, system=system)
    print("Finished rescaling")
    N_cycles = 10000
    MD_steps = 10000
    delta = 100
    N_snapshots = 100
    N_time = 2
    initial_simulation_time = time.time()*units.s
    
    
    pv_dict = {
        "time":[],
        "pressure_xyz_Pa":[]
    }
    
    if args.restartpv:
        print("\n*** Restarting simulation ***\n")
    else:
        print("\n*** Starting simulation ***\n")
        equilibration=True

    for cycle in range(N_cycles):    
        system.integrator.run(MD_steps)
        if args.rank == 0:
            if cycle % N_snapshots == 0:
                espressomd.io.writer.vtf.writevcf(system, outfile_PV)
           
        sim_time=units.Quantity(system.time, "reduced_time")

        # Store time
        pv_dict["time"].append(system.time)
        pressure_tensor = system.analysis.pressure_tensor()
        pv_dict["pressure_xyz_Pa"].append((pressure_tensor["total"][0,0]+pressure_tensor["total"][1,1]+pressure_tensor["total"][2,2])/3*units.Quantity(1, "reduced_energy/(reduced_length^3)").to("Pa").magnitude)
        
        # Report progress on a logarithmic scale to avoid spam on terminal
        if (cycle == int(N_time)):
            N_time=N_time*1.5
            misc.write_progress(step=cycle, 
                                total_steps=N_cycles, 
                                initial_simulation_time=initial_simulation_time, 
                                units=units)

        # Prevent the simulation to be killed by the cluster
        current_time=units.Quantity(time.time(),"s")
        time_expended=current_time-start_time
        if computing_time:
            fraction_completed=(time_expended/computing_time).magnitude
            if fraction_completed > 0.95:
                print(f"95% of the requested computing time has been consumed, exiting promtly at cycle {cycle}")
                break

    # Save the final state of the system in case we need to restart the simulation
    checkpoint_PV.save()
    print("\n*** Simulation finished ***\n")
    pv_df = pd.DataFrame.from_dict(pv_dict)
    if args.restartpv:
        prev_pv_df=analysis.read_csv_file(path=f'{args.structurefile}_fres-{args.fres}_pressure.csv')
        pv_df=analysis.merge_time_series_dfs(list_time_series_df=[prev_pv_df,pv_df])
    else: 
        param_to_add = {"fres": args.fres}
        param_to_add_df = pd.DataFrame(param_to_add, index=[0])
        param_df = pd.concat([param_df, param_to_add_df], axis=1)
        param_df.to_csv(f"{args.structurefile}_fres-{args.fres}_params.csv")
    
    pv_df.to_csv(f'{args.structurefile}_fres-{args.fres}_pressure.csv')
    outfile_PV.close()
    print("\n*** Data stored ***")
  
