import numpy as np
import os
import inspect
import pint
import sys
import argparse
import pandas as pd

import espressomd
from espressomd import interactions
import espressomd.polymer
import espressomd.io.writer.vtf  # pylint: disable=import-error
import espressomd.accumulators
import espressomd.observables
from itertools import combinations_with_replacement
from espressomd import checkpointing

current_dir= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
path_end_index=current_dir.find("modemus")
root_dir = current_dir[0:path_end_index]+"modemus"
sys.path.insert(0, root_dir)

from pmb_handy_tools.lib import misc_tools as misc

code_path= root_dir + "/code"
sys.path.insert(0, code_path)

structure_folder = root_dir + "/hydrogel_structures"

from lib import hydrogel

espressomd.assert_features(["WCA","THERMOSTAT_PER_PARTICLE"])

# Inputs from terminal
parser = argparse.ArgumentParser(description='Generate hydrogel structure')
## System inputs
parser.add_argument('--mpc', type=int, default=30, help='Monomers per fibril in the hydrogel')
parser.add_argument('--ncopies', type=int, default=0, help='Number of copies of the simulation box')
parser.add_argument('--dends', type=int, default=0, help='Number of dangling ends per chain')
parser.add_argument('--ldends', type=int, default=0, help='The length of dangling ends, if any')
parser.add_argument('--xlinks', action='store_true')
parser.add_argument('--no-xlinks', dest='xlinks', action="store_false")
parser.set_defaults(xlinks=False)

## Cluster-related inputs
parser.add_argument('--reqtime', type=str, default=None, help='Time requested in the cluster, format HH:MM:SS')
args = parser.parse_args ()


# Setup the units of the system
units=pint.UnitRegistry()
Kb=1.38064852e-23 * units.J / units.K
N_A=6.02214076e23 / units.mol
temperature=298.15 * units.K
kT=Kb*temperature

SEED = 42

unit_length=10*units.nanometer # Based on Bertula et al. 2019 and Martikainen et al. 2020
unit_time=100*units.nanosecond
unit_energy=Kb*temperature
unit_mass=unit_energy*unit_time**2/unit_length**2

units.define(f'reduced_length = {unit_length}')
units.define(f'reduced_time = {unit_time}')
units.define(f'reduced_mass = {unit_mass}')
units.define(f'reduced_energy = {unit_energy}')

sigma=units.Quantity(1, "reduced_length")

print("The reduced unit system is ")
print(f'reduced_length = {unit_length.to("nm")}')
print(f'reduced_time =   {unit_time.to("ns")}')
print(f'reduced_energy = {unit_energy.to("J")}')
print(f'reduced_mass =   {unit_mass.to("kg")} = {(unit_mass*N_A).to("kg / mol")}')

dt=units.Quantity(0.001, "reduced_time")
print(f"the time step is {dt.to('ns')}")

input_dict={"mpc":args.mpc,
            "ncopies":args.ncopies,
            "dends": args.dends,
            "ldends": args.ldends, 
            "xlinks": str(args.xlinks)
            }

output_name=misc.built_output_name(input_dict=input_dict)
# Set-up the checkpoint
checkpoint = checkpointing.Checkpoint(checkpoint_id=f"{output_name}", checkpoint_path=structure_folder)

# System parameters
#############################################################
system = espressomd.System(box_l=[10.0, 10.0, 10.0])
system.time_step = dt.to("reduced_time").magnitude
system.cell_system.skin = 0.4
checkpoint.register("system")
eta=units.Quantity(0.89, "mPa s")           # for water at 298.15
epsilon=1
lj=True

# Structure parameters
n_copies=args.ncopies # Number of copies of the simulation box
polymer_concentration=units.Quantity(1.64, "g/mL") # density of dry agarose taken fron Laurent1967
polymer_diameter=units.Quantity(10, "nm")
bond_length = units.Quantity(0.96, "reduced_length")
bendk = 10 # for the harmonic angle potential

# monomers per chain for diamond lattice
monomers_per_fibril = args.mpc
print(f"monomers per fibril {monomers_per_fibril}")

dangling_ends_per_fibril = args.dends
length_of_dangling_end = args.ldends
if args.dends > 0:
    if args.ldends == 0:
        raise ValueError("The length of the dangling ends are zero")
    print(f"dangling ends per fibril {dangling_ends_per_fibril}")
    print(f"length_of_dangling_end {length_of_dangling_end}")

types={"gel-n-nodes": 0,
       "gel-c-nodes": 1,
       "gel-n-monomers":2,
       "gel-c-monomers":3,
       "np":4, 
       "A":5}

charges = {
    types["gel-n-nodes"]: 0,
       types["gel-c-nodes"]: 0,
       types["gel-n-monomers"]:0,
       types["gel-c-monomers"]:0,
       types["np"]:0,
       types["A"]:0}

diameters={}

for key in types.keys():
    diameters[types[key]]=polymer_diameter.to("reduced_length")


box_l = (monomers_per_fibril + 1) * bond_length / (0.25 * np.sqrt(3))
box_l_long=box_l*(n_copies+1)
volume=box_l**2*box_l_long
system.box_l = [box_l.to("reduced_length").magnitude]*3

print(f"The size of the simulation box is {box_l.to('reduced_length')} x {box_l.to('reduced_length')} {box_l_long.to('reduced_length')}")
print(f"equivalent to  {box_l.to('nm')} x {box_l.to('nm')} x {box_l_long.to('nm')}")

number_of_polymer_beads=(n_copies+1)*(16*monomers_per_fibril+8) + (n_copies+1)*16*dangling_ends_per_fibril*length_of_dangling_end 
mass_per_fibril_bead=volume*polymer_concentration/number_of_polymer_beads

print(f"The mass of each polymer bead is {mass_per_fibril_bead.to('reduced_mass')} = {(mass_per_fibril_bead*N_A).to('kg / mol')}")

gamma_polymer_beads=6*np.pi*eta*polymer_diameter/(2*unit_mass)

print(f"gamma polymer bead {gamma_polymer_beads.to('1/ns')} reduced_gamma = {gamma_polymer_beads.to('1/reduced_time')}")


outfile = open(f'{structure_folder}/{output_name}/{output_name}.vtf', mode="w")

# Setup the interactions in the system
if lj:
    cutoff=2**(1./6.)*units('reduced_length')
    for type_pair in combinations_with_replacement(types.values(), 2):
        # using the 'Lorentz-Berthelot' combining rule
        
        diameter_sum=diameters[type_pair[0]]+diameters[type_pair[1]]
        offset=diameter_sum/2.-sigma
        pair_cutoff=cutoff        
        system.non_bonded_inter[type_pair[0],type_pair[1]].lennard_jones.set_params(epsilon = epsilon,
                                                                                    sigma   = sigma.to('reduced_length').magnitude,
                                                                                    cutoff  = pair_cutoff.to('reduced_length').magnitude,
                                                                                    offset  = offset.to('reduced_length').magnitude,
                                                                                    shift   = "auto",
                                                                                    )

# Create the polymer network
fene_bond = interactions.FeneBond(k=30, d_r_max=1.5, r_0=0) #Values based on Beyer et al. 2022
system.bonded_inter.add(fene_bond)
reversible_bond = interactions.HarmonicBond(k=100, r_0=bond_length.to('reduced_length').magnitude)
system.bonded_inter.add(reversible_bond)

# Bending potential
angle_harmonic = espressomd.interactions.AngleHarmonic(bend=bendk, phi0=np.pi)
system.bonded_inter.add(angle_harmonic)

print("Setting up hydrogel network")
espressomd.polymer.setup_diamond_polymer(system=system, 
                                    bond=fene_bond, 
                                    MPC=monomers_per_fibril)

hydrogel_map=hydrogel.set_up_non_cubic_system(n_copies=n_copies, 
                                        system=system, 
                                        bond_object=fene_bond, 
                                        type_map=types, 
                                        bond_length=bond_length.magnitude)
hydrogel.add_bending_potential(angle_object=angle_harmonic, type_map=types, mpc=monomers_per_fibril, hydrogel_map=hydrogel_map, system=system)
checkpoint.register("hydrogel_map")
if args.dends > 0:
    print("Adding dangling ends")
    hydrogel.add_dangling_ends(dangling_ends_per_fibril=dangling_ends_per_fibril, length_of_dangling_end=length_of_dangling_end, monomers_per_fibril=monomers_per_fibril, hydrogel_map=hydrogel_map, bond_length=bond_length.magnitude, irreversible_bond_object=fene_bond, types=types, angle_object=angle_harmonic, charges=charges, system=system)
system.part.all().gamma=[gamma_polymer_beads.to("1/reduced_time").magnitude]*3
system.part.all().mass=mass_per_fibril_bead.to("reduced_mass").magnitude

espressomd.io.writer.vtf.writevsf(system, outfile)
espressomd.io.writer.vtf.writevcf(system, outfile)
#############################################################
#      Warmup                                            #
#############################################################
system.thermostat.turn_off()
# minimize energy using min_dist as the convergence criterion
system.integrator.set_steepest_descent(f_max=0, 
                                    gamma=1,
                                    max_displacement=0.001)
system.integrator.run(10000)

print(f"minimization: {system.analysis.energy()['total']:+.2e}")
print()
system.integrator.set_vv()

# activate thermostat
system.thermostat.set_langevin(kT=kT.to("reduced_energy").magnitude, 
                            gamma=1, 
                            seed=SEED)

espressomd.io.writer.vtf.writevcf(system, outfile)

if args.xlinks and args.dends > 0:
    # #Rescaling box size
    hydrogel.rescaling_cubic_box_to_desired_box_l(relative_box_l=0.6, system=system)
    print("Doing crosslinking")
    #TODO: Expand the box to original size again
    
espressomd.io.writer.vtf.writevcf(system, outfile)

V=units.Quantity(system.volume(), "reduced_length**3")
hydrogel_volume_fraction=4/3*np.pi*number_of_polymer_beads*polymer_diameter.to("nm")**3/V.to("nm**3")

agarose_dict={
            "mpc": args.mpc,
            "ncopies": args.ncopies,
            "dends": args.dends,
            "ldends": args.ldends,
            "xlinks": str(args.xlinks),
            "agarose_concentration_mg_mL": [polymer_concentration.to("mg/mL").magnitude],
            "agarose_bead_diameter_nm": [polymer_diameter.to("nm").magnitude],
            "agarose_bead_mass_kg_mol": [(mass_per_fibril_bead*N_A).to('kg / mol').magnitude],
            "agarose_gamma_ns": [gamma_polymer_beads.to('1/ns').magnitude], 
            "lj": [lj],
            "boxl_nm": [box_l.to('nm').magnitude],
            "agarose_volume_fraction":hydrogel_volume_fraction.magnitude,
            "eta": eta.to("mPa s").magnitude,
            "bend_k": bendk,
            "bond_length_nm": bond_length.to("nm").magnitude,
            "dangling_ends_per_chain": dangling_ends_per_fibril,
            "length_of_dangling_end": length_of_dangling_end,
            "contour_length_nm": hydrogel.calculate_contour_length_between_nodes(monomers_per_fibril=monomers_per_fibril, bond_length=bond_length).to("nm").magnitude}


agarose_df = pd.DataFrame.from_dict(agarose_dict)
agarose_df.to_csv(f'{structure_folder}/{output_name}/{output_name}_params.csv',index=False)

outfile.close()
checkpoint.save()

print("Structure saved")