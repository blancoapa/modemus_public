def add_NPs_to_system(system, particle_properties_dict, initialization_type):
    """
    Calls the function of wanted type of NP initialization

    Args:
        system (`cls`): System object of the espresso library
        particle_properties_dict (`dict`): Dictionary containing properties of particles and simulation box
        initialization_type (`str`): Can either be "random" or "yz-lattice"
        

    Authors:
        - Pablo M. Blanco, Norwegian University of Science and Technology (NTNU)
        - Marit S. Otervik, Norwegian University of Science and Technology (NTNU)
        
    """

    if initialization_type == "random":
        do_random_initialization(system, particle_properties_dict)
    elif initialization_type == "yz-lattice":
        do_yz_lattice_initialization(system, particle_properties_dict)
    return
    

def do_random_initialization(system, particle_properties_dict):
    """
    Gives intitial properties to the NPs and placing them at random positions in simulation box.

    Args:
        system (`cls`): System object of the espresso library
        particle_properties_dict (`dict`): Dictionary containing properties of particles and simulation box

    Note:
        - For simulations studying mean square displacecment of the NPs

    Authors:
        - Pablo M. Blanco, Norwegian University of Science and Technology (NTNU)
        - Charlotte B. Hunskår, Norwegian University of Science and Technology (NTNU)
        - Marit S. Otervik, Norwegian University of Science and Technology (NTNU)
        
    """
    import numpy as np
    
    inserted_np_id=len(system.part.all().id)

    box_volume = (particle_properties_dict["box_volume"]).to("mL")
    NP_concentration = particle_properties_dict["np_concentration"].to("g/mL")
    NP_mass = particle_properties_dict["np_mass"].to("g")
    NPs_number=(box_volume * NP_concentration / NP_mass).magnitude

    if NPs_number < 1:
        n_copies = particle_properties_dict["box_copies"]
        
        NPs_number = n_copies + 1

    for _ in range(int(NPs_number)):  
        system.part.add(pos  = np.random.random((1, 3))[0]*np.copy(system.box_l), 
                        type = particle_properties_dict["np_type"],
                        gamma= particle_properties_dict["np_gamma"],
                        mass = particle_properties_dict["np_mass"].to("reduced_mass").magnitude,
                        id   = inserted_np_id)
        np_overlapping=True
        while np_overlapping:
            np_overlapping=prevent_NP_overlapping(system=system, 
                                                inserted_np_id=inserted_np_id,
                                                particle_properties_dict=particle_properties_dict)
            if np_overlapping:
                inserted_part= system.part.by_id(inserted_np_id)
                inserted_part.pos=np.random.random((1, 3))[0]*np.copy(system.box_l)
        inserted_np_id+=1
    return

def do_yz_lattice_initialization(system, particle_properties_dict):
    """
    Gives intitial  properties to the NPs and placing them in a lattice structure at x=0.

    Args:
        system (`cls`): System object of the espresso library
        particle_properties_dict (`dict`): Dictionary containing properties of particles and simulation box

    Note:
        - For simulations studying the concentration profile of NPs

    Authors:
        - Marit S. Otervik, Norwegian University of Science and Technology (NTNU)
        
    """
    import numpy as np
    inserted_np_id=len(system.part.all().id)
    
    NP_diameter = particle_properties_dict["np_diameter"].to("reduced_length").magnitude
    box_l_y = np.copy(system.box_l)[1]
    box_l_z = np.copy(system.box_l)[2]

    step_size_y = NP_diameter + 1 
    step_size_z = NP_diameter + 1
    if (2*step_size_y >= box_l_y) or (2*step_size_z >= box_l_z):
        raise ValueError("The simulation box is too small for NP lattice")

    yv, zv = np.mgrid[0:(box_l_y-1):step_size_y, 0:(box_l_z-1):step_size_z]
    yz_lattice = np.c_[yv.ravel(), zv.ravel()]

    x1_positions = np.random.random(len(yz_lattice))*NP_diameter
    x2_positions = np.random.random(len(yz_lattice))*NP_diameter + 2*NP_diameter

    x1_lattice = np.column_stack((x1_positions, yz_lattice))
    x2_lattice = np.column_stack((x2_positions, yz_lattice))

    double_yz_lattice = np.concatenate((x1_lattice, x2_lattice))

    np_overlapping = False

    for lattice_position in double_yz_lattice:
        if not np_overlapping:
            system.part.add(pos = lattice_position, 
                            type = particle_properties_dict["np_type"],
                            gamma= particle_properties_dict["np_gamma"],
                            mass = particle_properties_dict["np_mass"].to("reduced_mass").magnitude,
                            id   = inserted_np_id)

        if np_overlapping:
                inserted_part= system.part.by_id(inserted_np_id)
                inserted_part.pos  = lattice_position

        np_overlapping=prevent_NP_overlapping(system=system, 
                                                inserted_np_id=inserted_np_id,
                                                particle_properties_dict=particle_properties_dict)
        if not np_overlapping:
            inserted_np_id += 1
    return

def prevent_NP_overlapping(system, inserted_np_id, particle_properties_dict):
    """
    Prevents newly generated nanoparticles (NPs) to overlap other NPs in the simulation box

    Args:
        system (`cls`): System object of the espresso library
        particle_properties_dict (`dict`): Dictionary containing properties of particles and simulation box

    Authors:
        - Pablo M. Blanco, Norwegian University of Science and Technology (NTNU)  
        - Marit S. Otervik, Norwegian University of Science and Technology (NTNU)      
    """
    inserted_part= system.part.by_id(inserted_np_id)
    NP_diameter = particle_properties_dict["np_diameter"].to("reduced_length").magnitude
    np_overlapping=False

    for part in system.part.all():
        part_type = part.type
        if part.type == particle_properties_dict["np_type"]:
            part_diameter = NP_diameter
        else:
            part_diameter = particle_properties_dict["polymer_bead_diameter"].to("reduced_length").magnitude
        interaction_distance=(NP_diameter + part_diameter)/2

        if part.id == inserted_np_id:
            continue
        elif system.distance(part, inserted_part) < interaction_distance:
            np_overlapping=True
            break

    return np_overlapping


def calculate_concentration_histogram(system, start, stop, step_size, particle_properties_dict):
    """
    Calculates a histogram of NP positions at the current simulation timestep.

    Args:
        system (`cls`): System object of the espresso library
        start (`float`): Start value for bins in reduced length
        stop (`float`):  End value for bins in reduced length
        step_size(`float`): Width of histogram bins in reduced length
        particle_properties_dict (`dict`): Dictionary containing properties of particles and simulation box

    Authors:
        - Marit S. Otervik, Norwegian University of Science and Technology (NTNU)

    Note:
    The bins will be symmetric around x=0, but start and stop can be different.
        
    """
    import numpy as np

    if start > 0:
        start = 0

    pos_histogram_steps = int((stop-0)/step_size)+1
    pos_histogram_bins = np.linspace(0,stop,num=pos_histogram_steps,endpoint=True)

    neg_histogram_bins = pos_histogram_bins[pos_histogram_bins<abs(start)][1:]*-1
    histogram_bins = np.concatenate((np.flip(neg_histogram_bins),pos_histogram_bins))

    NP_type = particle_properties_dict["np_type"]
    NP_pos_x = system.part.select(type=NP_type).pos[:,0]
    histogram = np.histogram(NP_pos_x, bins=histogram_bins)

    return histogram

