#Swelling eq. dictionary, with bending potential k=10
mpc_to_swelling_eq={
    30 : 0.58,
    40 : 0.54,
    50 : 0.52
}

#Swelling eq. dictionary, without bending potential
mpc_to_swelling_eq_wo_bendingpot ={
    30 : 0.324,
    40 : 0.286,
    50 : 0.260
}


def get_swelling_eq_from_mpc(monomers_per_fibril):
    """Get the swelling equlibrium value given by the relative box length at equlibrium, given the number of monomers per fibril.
    If the monomers per fibril given does not have a swelling equlibrium value, it raises a ValueError.

    Args:
        monomers_per_fibril (`int`): Number of monomers per fibril

    Raises:
        ValueError: The number of monomers per fibril does not have a value for the swelling equilibrium. It prints the possible values, and that a value can be found by inputting rescaling factors and finding where the pressure of the system is zero. 

    Returns:
        `float`: The relative box length giving the swelling equilibrium
    """
    try:
        relative_box_l = mpc_to_swelling_eq[monomers_per_fibril]
    except:
        raise ValueError(f"The number of monomers per fibril (mpc) does not have a value for the swelling equilibrium. Possible mpc are {mpc_to_swelling_eq.keys()}. \n To get the swelling eq. of the new mpc, input rescaling factors manually to find where the pressure is zero. ")
    
    return relative_box_l


def get_particles_in_hydrogel(type_map, system):
    """
    Get the ids of the particles in the system that are part of the hydrogel.

    Args:
        type_map (`dict`): Dictionary mapping the espresso types
        system (`cls`): system object of the espresso library 

    Returns:
        `list `: List containing the ids of the particles in the hydrogel, sorted by default in ascending order

    Note:
        - Assumes that the particle types of the hydrogel contain the keyword gel in their label
    
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
        - Pablo M. Blanco, Norwegian University of Science and Technology (NTNU)
    """
    particles_in_hydrogel = []
    for key in type_map.keys():
        if "gel" in key:
            particles_in_hydrogel+=list(system.part.select(type=type_map[key]).id)
    return particles_in_hydrogel


def bond_particles_in_copies(initial_particles_in_hydrogel, number_of_copies, bond_object, system):
    """
    Bonds all particles in the copies of the original simulation box keeping the original relative conectivity
    
    Args:
        initial_particles_in_hydrogel (`list`): Initial list of the ids of the particles in the hydrogel
        system (`cls`): System object of the espresso library 
        number_of_copies (`int`): Number of copies to be made of the lattice
        bond_object (`cls`): Bond object of the espresso library 
        
    Note: 
        - This ensures periodicity in y and z direction

    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    initial_particle_number = len(initial_particles_in_hydrogel)
    for id in initial_particles_in_hydrogel: 
        particle = system.part.by_id(id)
        for copies in range(1, number_of_copies+1):
            particle_i = system.part.by_id(initial_particle_number*copies+id)
            for bond in particle.bonds:
                    particle_i.add_bond((bond_object, system.part.by_id(bond[1]+initial_particle_number*copies)))
    return

def map_hydrogel(particles_in_hydrogel, initial_number_of_particles, n_copies, bond_length, system):
    """
    Maps the particles in the hydrogel which are in the nodes and which in the boundaries of the simulation box
    
    Args:
        particles_in_hydrogel (`list`): List of ids of particles in the hydrogel
        initial_number_of_particles (`int`): The number of particles in the hydrogel before translating
        n_copies (`int`): Number of copies to be made of the lattice
        bond_length (`float`): The bond length between the hydrogel particles
        system (`cls`): System object of the espresso library 

    Returns:
        `dictionary`: the hydrogel map with particle ids of nodes and near boundaries, and neighbouring nodes and opposite nodes
        
    Note:
        - If particles is near several of the planes, it is included in its x-plane.
        - Neighbouring nodes are the nodes connected to a node through 1 monomer chain (not bonding neighbour).
        - A neighbouring node is only included one way. Example if 0 and 1 is neighbour, 1 is included as 0's neighbour, but not the other way around.
        - Neighbouring and opposite nodes are decided by how the lattice is created by espresso.
        - Only one node per lattice copy is included with a opposite, to not get corrolation when calculating pore size
        - Structure of hydrogel_map = {
            "nodes": [id_node_1, ...id_node_n], 
            "boundaries" : {
                "x_0" : []
                ...
                "x_n" : []
                "y_new" : []
                "z_new" : []
                }
            "neighbouring nodes" : {
                id_node_1 : []
                id_node_2 : []
                ...
                }
            "opposite nodes" : {
                id_node_1 : id_opposite_node_1
                id_node_2 : id_opposite_node_2
                ...
                }
            }
        - `x_n`: particle ids in the x-planes where the x coordinate of the position is near x = n*a. 
        - `y_new`: particle ids whose y coordinate is near either 0 or (n_copies+1)*a
        - `z_new`: particle ids whose z coordinates is near either 0 or (n_copies+1)*a

    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
        - Pablo M. Blanco, Norwegian University of Science and Technology (NTNU)
    """
    import numpy as np
    hydrogel_map = {
                    "nodes": [],
                    "boundaries" : {},
                    "neighbouring_nodes" : {},
                    "opposite_nodes" : {}
                    }
    for copy in range(n_copies+2):
        hydrogel_map["boundaries"][f"x_{copy}"] = []
    hydrogel_map["boundaries"]["y_new"] = []
    hydrogel_map["boundaries"]["z_new"] = []
    
    box_short_edge=system.box_l[1]
    for id in particles_in_hydrogel:
        particle = system.part.by_id(id)
        particle_ids_in_x_bondaries=[]
        for copy in range(n_copies+2):
            particle_nearby_xboundary = np.isclose(particle.pos[0], (copy)*box_short_edge, atol=bond_length)
            if particle_nearby_xboundary:
                hydrogel_map["boundaries"][f"x_{copy}"].append(particle.id)
                particle_ids_in_x_bondaries.append(particle.id)
                break
        axis={"y_new":1,
              "z_new":2}
        for axis_key in axis.keys():
            axis_index=axis[axis_key]
            particle_near_0boundary = np.isclose(particle.pos[axis_index], 0, atol=bond_length)
            particle_near_Lboundary = np.isclose(particle.pos[axis_index], box_short_edge, atol=bond_length)
            particle_near_boundary = particle_near_0boundary or particle_near_Lboundary
            if particle_near_boundary:
                if particle.id not in particle_ids_in_x_bondaries:
                    hydrogel_map["boundaries"][axis_key].append(particle.id)
                    
                    
    # Creation of neigbouring nodes
    for copy in range(n_copies+1):
        factor = initial_number_of_particles * copy
        previous_lattice_factor = initial_number_of_particles * n_copies if copy == 0 else initial_number_of_particles * (copy - 1)
        next_lattice_factor = initial_number_of_particles * (copy + 1) if copy != n_copies else 0
        hydrogel_map["neighbouring_nodes"][0 + factor] = [1 + factor]
        hydrogel_map["neighbouring_nodes"][1 + factor] = [2 + factor, 3 + factor, 4 + factor]
        hydrogel_map["neighbouring_nodes"][2 + factor] = [1 + factor, 5 + factor, 6 + factor, 7 + factor]
        hydrogel_map["neighbouring_nodes"][3 + factor] = [1 + factor, 6 + factor, 7 + previous_lattice_factor, 5 + previous_lattice_factor]
        hydrogel_map["neighbouring_nodes"][4 + factor] = [1 + factor, 5 + factor, 6 + factor, 7 + factor]
        hydrogel_map["neighbouring_nodes"][5 + factor] = [2 + factor, 4 + factor, 3 + next_lattice_factor]
        hydrogel_map["neighbouring_nodes"][6 + factor] = [0 + factor, 2 + factor, 3 + factor, 4 + factor]
        hydrogel_map["neighbouring_nodes"][7 + factor] = [2 + factor, 4 + factor, 3 + next_lattice_factor]
    
    #Creation of opposite nodes
    for copy in range(n_copies+1):
        hydrogel_map["opposite_nodes"][3 + initial_number_of_particles * copy] = 7 + initial_number_of_particles * copy
    
    return hydrogel_map

def calculate_distance(particle_id1, particle_id2, system):
    """
    Calculates the distance between the particle with id 'particle_id1' and id 'particle_id2'

    Args:
        particle_id1 (`int`): Id of particle 1
        particle_id2 (`int`): Id of particle 2
        system (`cls`): System object of the espresso library 

    Returns:
        `int`: Distance between particle_id1, particle_id2
    
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    particle1 = system.part.by_id(particle_id1)
    particle2 = system.part.by_id(particle_id2)
    return system.distance(particle1, particle2)

def search_bond_partner_id(bond):
    """
    Searching for the id of the bond partner in the specific bond

    Args:
        bond (`tuple`): Bond type in the espresso library, tuple(bond type, bond_partner_id)

    Returns:
        `int`: id of the bond partner
    
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    return bond[1]

def search_bonded_particle_ids(particle_id, system):
    """
    Searches for all the bond partners of the particle

    Args:
        particle_id (`int`): id of particle you want to finds bond partners to
        system (`cls`): System object of the espresso library 

    Returns:
        `list`: List of ids of bond partners
    
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    return [bond[1] for bond in system.part.by_id(particle_id).bonds]

def bond_particles_across_boundaries(particle_ids_border1, particle_ids_border2, bond_object, bond_length, system):
    """
    Bond particles across boundaries (border 1 and border 2)

    Args:
        particle_ids_border1 (`list`): Particle ids on the first border
        particle_ids_border2 (`list`): Particle ids on the second border
        bond_object (`cls`): Bond object of the espresso library 
        bond_length (`float`): The bond length between the hydrogel particles
        system (`cls`): System object of the espresso library 

    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    for particle_id in particle_ids_border1:
        for particle_id_partner in particle_ids_border2:
            if particle_id == particle_id_partner:
                continue
            dist = calculate_distance(particle_id1=particle_id, 
                                      particle_id2=particle_id_partner,
                                      system=system)
            bonded_particle_ids=search_bonded_particle_ids(particle_id=particle_id, 
                                                           system=system)
            if (round(dist,3) <= bond_length) and (particle_id_partner not in bonded_particle_ids): 
                system.part.by_id(particle_id).add_bond((bond_object, system.part.by_id(particle_id_partner))) 
    return
    
def unbond_particles_across_boundaries(particle_ids_border1, particle_ids_border2, system):
    """
    Unbond particles across boundaries (border 1 and border 2)

    Args:
        particle_ids_border1 (`list`): Particle ids on the first border
        particle_ids_border2 (`list`): Particle ids on the second border
        system (`cls`): System object of the espresso library 

    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    for particle_id in particle_ids_border1:
        for bond in system.part.by_id(particle_id).bonds:
            bond_partner_id = search_bond_partner_id(bond)
            if bond_partner_id in particle_ids_border2:
                system.part.by_id(particle_id).delete_bond(bond)
    return
    
def translate_hydrogel_lattice(particles_in_hydrogel, n_copies, initial_number_of_particles, system):
    """
    Translates the lattice in x-direction n_copies times 

    Args:
        particles_in_hydrogel (`list`): List of ids of particles in the hydrogel
        n_copies (`int`): Number of copies to be made of the lattice
        initial_number_of_particles (`int`): The number of particles in the hydrogel before translating
        system (`cls`): system object of the espresso library 

    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    box_short_edge=system.box_l[1]
    for id in particles_in_hydrogel:
        particle = system.part.by_id(id)
        for copies in range(1, n_copies+1):
            system.part.add(pos=[particle.pos[0] + copies*box_short_edge, particle.pos[1], particle.pos[2]], 
                            type=particle.type, 
                            id = particle.id + initial_number_of_particles*copies)
    return

def set_up_non_cubic_system(n_copies, bond_object, bond_length, type_map, system):
    """
    Sets up a non-cubic system by translating a cubic lattice n_copies times in the x-direction and bonding all particles to make it periodic in the now elongated lattice

    Args:
        n_copies(`int`): Number of periodic copies to be created
        bond_object (`cls`): Bond object of the espresso library
        bond_length (`float`): The bond length between the hydrogel particles
        type_map (`dict`): Dictionary mapping the espresso types
        system(`cls`): System object of the espresso library 
        
    Returns:
        hydrogel_map(`dict`): map of the topology of the hydrogels. See `map_hydrogel` for additional details.
    
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
        - Pablo M. Blanco, Norwegian University of Science and Technology (NTNU)
    """
    import numpy as np
    # Creation of hydrogel map
    initial_particles_in_hydrogel = get_particles_in_hydrogel(type_map=type_map, system=system) #Called before translating
    initial_number_of_particles = len(initial_particles_in_hydrogel)
    translation_vector = np.array([system.box_l[0], 0, 0])
    system.box_l=system.box_l+n_copies*translation_vector

    # Translation
    translate_hydrogel_lattice(particles_in_hydrogel=initial_particles_in_hydrogel, n_copies=n_copies, initial_number_of_particles=initial_number_of_particles, system=system)
    
    
    bond_particles_in_copies(initial_particles_in_hydrogel=initial_particles_in_hydrogel, bond_object=bond_object, number_of_copies=n_copies, system=system)



    
    hydrogel_map = map_hydrogel(particles_in_hydrogel=get_particles_in_hydrogel(type_map=type_map, system=system), initial_number_of_particles=initial_number_of_particles, n_copies=n_copies, 
                                system=system,
                                bond_length=bond_length)

    for n in range(n_copies+1):
        unbond_particles_across_boundaries(particle_ids_border1=hydrogel_map["boundaries"][f"x_{n}"], 
                                           particle_ids_border2=hydrogel_map["boundaries"][f"x_{n+1}"], 
                                           system=system)
        bond_particles_across_boundaries(particle_ids_border1=hydrogel_map["boundaries"][f"x_{n}"], 
                                         particle_ids_border2=hydrogel_map["boundaries"][f"x_{n}"], 
                                         system=system,
                                         bond_object=bond_object, 
                                         bond_length=bond_length)
       
    bond_particles_across_boundaries(particle_ids_border1=hydrogel_map["boundaries"]["x_0"], 
                                     particle_ids_border2=hydrogel_map["boundaries"][f"x_{n_copies+1}"], 
                                     system=system,
                                     bond_object=bond_object,
                                     bond_length=bond_length)
    
    node_types_list=[]
    for key in type_map.keys():
        if "node" in key:
            node_types_list.append(type_map[key])
    hydrogel_map["nodes"] =[]
    for particle in system.part:
        if particle.type in node_types_list:
            hydrogel_map["nodes"].append(particle.id)

    return hydrogel_map


def fix_boundary_beads_in_hydrogel(hydrogel_map, n_copies, system):
    """
    Fixes the particles in the boundaries of the simulation box in place (x_0, x_n, y_new, z_new)
    Can for example be used it is not wanted that they are affected by an applied force. 

    Args:
        hydrogel_map (`dict`): Dictionary with ids of nodes and boundary particles
        n_copies(`int`): Number of periodic copies to be created
        system(`cls`): system object of the espresso library 
        
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    boundaries = ["x_0", f"x_{n_copies+1}", "y_new", "z_new"]
    
    for dimension in boundaries:
        for node_id in hydrogel_map["boundaries"][dimension]:
            system.part.by_id(node_id).fix = [True, True, True]


def fix_all_nodes_in_hydrogel(hydrogel_map, system):
    """
    Fixes all the nodes in the hydrogel in place, based on the particle id's listed in hydrogel_map["nodes"].

    Args:
        hydrogel_map (`dict`): Dictionary with ids of nodes and boundary particles
        system(`cls`): system object of the espresso library 
        
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    for node_id in hydrogel_map["nodes"]:
        system.part.by_id(node_id).fix = [True, True, True]
    

def calculate_pore_sizes(hydrogel_map, system):
    """
    Calculates the instantaneous pore sizes in the lattice based on the opposite nodes found in hydrogel_map["opposite_nodes"]

    Args:
        hydrogel_map (`dict`): Dictionary with ids of nodes and boundary particles
        system(`cls`): system object of the espresso library 

    Returns:
        `list[float]`: A list of the instantaneous pore sizes in the lattice. Pore size has unit reduced length
    
    Note:
        See the structure of `hydrogel_map` in the docs of `map_hydrogel`. 
    
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    pore_sizes = []
    for node_id, opposite_node_id in hydrogel_map["opposite_nodes"].items():
        pore_sizes.append(calculate_distance(particle_id1=node_id, particle_id2=opposite_node_id, system=system))
    return pore_sizes


def calculate_end_to_end_distances(hydrogel_map, system):
    """
    Calculates the end to end distance in the hydrogel based on the neighbouring nodes found in hydrogel_map["neighbouring_nodes"]

    Args:
        hydrogel_map (`dict`): Dictionary with ids of nodes and boundary particles
        system(`cls`): system object of the espresso library 

    Returns:
        `list[float]`: The end to end distance in units of reduced length
    
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    end_to_end_distances = []
    for node_id, list_of_neighbours in hydrogel_map["neighbouring_nodes"].items():
        for neighbouring_node_id in list_of_neighbours:
            end_to_end_distances.append(calculate_distance(particle_id1=node_id, particle_id2=neighbouring_node_id, system=system))
    return end_to_end_distances
    

def calculate_persistence_length_between_nodes(monomers_per_fibril, hydrogel_map, bond_length, system):
    """
    Calculates the persistence length of the fibrils between nodes according to the Flory definition. 
    The fibrils consist of monomers_per_fibril number of beads and to remove end effects, the persistence length is calculated by averaging over the persistence length calculated from bead #1 to bead #monomers_per_fibril/2.
    This calculation is based on paper by Bacová et al.(2012): Double-exponential decay of orientational correlations in
    semiflexible polyelectrolytes, DOI 10.1140/epje/i2012-12053-6
    
    Returns a list of the persistence lengths between the fibrils.
    
    Args:
        monomers_per_fibril (`int`): Number of monomers per fibril in the diamond lattice
        hydrogel_map ('dict'): Dictionary with ids of nodes and boundary particles
        bond_length (`float`): The bond length between the hydrogel particles
        system(`cls`): system object of the espresso library 

    Returns:
        `list[float]`: A list containing the persistence length of different fibrils in the lattice
    
    Note:
        The word 'beads' are used to name the particles in between two nodes (the fibril)
        The persistence length is only calculated in ascending id order between two nodes to not calculate one fibril twice
        The lattice is assumed to be a diamond lattice
        The persistence length is not calculated for all the fibrils because of periodic boundary conditions (only using 5 of the 8 nodes)
        
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    import numpy as np
    from math import ceil
    from statistics import mean
   
    persistence_lengths = []

    for fibril_particles in find_fibrils(start_node_id=0, end_node_id=4, monomers_per_fibril=monomers_per_fibril, hydrogel_map=hydrogel_map, system=system):
            persistence_iterations = []
            for start_bead in range(0, ceil(len(fibril_particles)/2)):
                l_1 = system.part.by_id(fibril_particles[start_bead+1]).pos - system.part.by_id(fibril_particles[start_bead]).pos 
                sum = 0
                for index in range(start_bead, len(fibril_particles)-1):
                        l_i = system.part.by_id(fibril_particles[index+1]).pos - system.part.by_id(fibril_particles[index]).pos 
                        sum += np.dot(l_i, l_1)
                persistence_iterations.append(sum/bond_length.magnitude)
            persistence_lengths.append(mean(persistence_iterations))

    return persistence_lengths

def find_fibrils(start_node_id, end_node_id, monomers_per_fibril, hydrogel_map, system):
    """Finding the particle ids for the particles in each fibril and making it into a consecutive list. Returns a list with the lists of each fibril

    Args:
        start_node_id (`int`): The particle id of the start node. Typically 0.
        end_node_id (`int`): The particle id of the end node. Defines between how many nodes you want to find fibrils between.
        monomers_per_fibril (`int`): Number of monomers per fibril
        hydrogel_map ('dict'): Dictionary with ids of nodes and boundary particles
        system(`cls`): System object of the espresso library

    Returns:
        `list[list[int]]`: List containing the lists of consecutive particle ids of each fibril.
    """
    
    fibrils = []
    used_node_pairs = {}
    for node_id in range(start_node_id, end_node_id+1):
        used_node_pairs[node_id] = []
    
    for startnode_id in range(start_node_id,end_node_id+1):
        for bead_id in search_bonded_particle_ids(particle_id=startnode_id, system=system):
            if(not are_particles_in_ascending_order(particle_id=bead_id, monomers_per_fibril=monomers_per_fibril, system=system)):
                continue
            last_bead_id = bead_id + monomers_per_fibril -1
            end_node_id = find_bonded_node(hydrogel_map=hydrogel_map, particle_id=last_bead_id, system=system)
            if (end_node_id in used_node_pairs[startnode_id]):
                continue
            used_node_pairs[startnode_id].append(end_node_id)
            fibril_particles = []
            fibril_particles.append(startnode_id)
            for monomers in range(0,monomers_per_fibril):
                fibril_particles.append(bead_id + monomers)
            fibril_particles.append(end_node_id)
            fibrils.append(fibril_particles)
    return fibrils

def find_bonded_node(hydrogel_map, particle_id, system):
    """
    Find the id of the node that is bonded to the particle with id particle_id.

    Args:
        hydrogel_map ('dict'): Dictionary with ids of nodes and boundary particles
        particle_id ('int'): Id of the particle bonded to the node_id returned
        system ('cls'): system object of the espresso library

    Returns:
        'int': the id of the node that the particle_id bead is bond to
        
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    for node_id in hydrogel_map["nodes"]:
        bonded_particles_ids = search_bonded_particle_ids(particle_id=node_id, system=system)
        if particle_id in bonded_particles_ids:
            return node_id
        
def are_particles_in_ascending_order(particle_id, monomers_per_fibril, system):
    """
    Checks if a particle with id particle_id is bonded to a particle with an id particle_id+1, and if so the particles are ordered in ascending order and True is returned. 
    If not, returns false.

    Args:
        particle_id ('int'): particle id of a bead
        system ('cls'): system object of the espresso library

    Returns:
        boolean: Returns True if the beads are in ascending order, False if not
        
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """

    if (particle_id < 16*monomers_per_fibril+8 -1):
        if (particle_id in search_bonded_particle_ids(particle_id=particle_id+1, system=system)):
            return True
    else:
        return False 
    
       
def calculate_contour_length(hydrogel_map, n_copies, system):
	"""
    Calculates the contour length of the hydrogel based on the end-to-end distance of a segment in the hydrogel multiplied with the number of segments the persistence length is calculated from. 

    Args:
        hydrogel_map (`dict`): Dictionary with ids of nodes and boundary particles
        n_copies(`int`): Number of periodic copies to be created
        system(`cls`): system object of the espresso library 

    Returns:
        `float`: The contour length in reduced units
        
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
	import numpy as np
	chain_nodes = [3,6,4,7]
	number_of_segments = (n_copies + 2) *  len(chain_nodes)
	length_of_segment = np.mean(calculate_end_to_end_distances(hydrogel_map=hydrogel_map, system=system))
    
	return number_of_segments * length_of_segment
      
def calculate_contour_length_between_nodes(monomers_per_fibril, bond_length):
    """
    Calculates the contour length of the hydrogel in between nodes, ie. of one fibril, based on the number of monomers per fibril.
    
    Args:
        monomers_per_fibril (`int`): Number of monomers per fibril
        bond_length (`float`): The bond length between the hydrogel particles

    Returns:
        `float`: The contour length in between nodes of one fibril in reduced units
        
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU)
    """
    return (monomers_per_fibril + 2 - 1)*bond_length
    
       
def rescaling_cubic_box_to_desired_box_l(relative_box_l, system, reuse_forces=False):
    """
    Function to rescale the simulation box and particles down to a final box volume based on the given relative_box_l. 
    The step size of each rescaling is equal to sigma, being the reduced unit of length.
    
    Parameters based on the paper "Simulations Explain the Swelling Behavior of Hydrogels with Alternating Neutral and Weakly Acidic Blocks" from Beyer et al. (2022), 
    doi: 10.1021/acs.macromol.2c01916

    Args:
        relative_box_l (`float`): The desired relative length of the simulation box, equal to final_box_l/initial_box_l
        system (`cls`): system object of the espresso library 
        reuse_forces (`bool`): If reusing forces from a saved checkpoint. Defaults to False
    
    Note:
        The relative_box_l must yield relative_box_l*system.box_l > 2*system.cell_system.range in all directions.
    
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU), 2024
        - Pablo M. Blanco, Norwegian University of Science and Technology (NTNU), 2024
    """
    import numpy as np
    step_size = 1 #sigma, reduced units
    box_sizes = {"initial":{"x":system.box_l[0],
                            "y":system.box_l[1],
                            "z":system.box_l[2],},
                 "final":  {"x":relative_box_l*system.box_l[0],
                            "y":relative_box_l*system.box_l[1],
                            "z":relative_box_l*system.box_l[2],}}
    box_dirs=["x","y","z"]
    for dir in box_dirs:
        if (box_sizes["final"][dir]/2 < system.cell_system.interaction_range):  
            raise ValueError(f"The final box length of {box_sizes['final'][dir]} is less than the cell system interaction range limit of {system.cell_system.interaction_range*2}. Increase the relative box length.")
        number_of_steps_to_final_box_l = int(box_sizes["initial"][dir] - box_sizes["final"][dir])/step_size    
        for _ in np.arange(0,number_of_steps_to_final_box_l):
            system.change_volume_and_rescale_particles(d_new=system.box_l[box_dirs.index(dir)] - step_size, 
                                                       dir=dir)
            system.integrator.run(1000, reuse_forces=reuse_forces)
        system.change_volume_and_rescale_particles(d_new=box_sizes["final"][dir], 
                                                   dir=dir)
    return 


def add_dangling_ends(dangling_ends_per_fibril, length_of_dangling_end, monomers_per_fibril, hydrogel_map, bond_length, irreversible_bond_object, types, charges, system, angle_object=None):
    """
    Adding dangling ends to the fibrils in the hydrogel. 
    If the angle_object is not None, a bending potential defined by the angle_object is applied to the dangling end

    Args:
        dangling_ends_per_fibril (`int`): Number of dangling ends per fibril in the hydrogel
        length_of_dangling_end (`int`): Number of beads the dangling ends should consist of
        monomers_per_fibril (`int`): Number of monomers per fibril in the hydrogel
        hydrogel_map (`dict`): Dictionary with ids of nodes and boundary particles
        bond_length (`float`): The bond length between the hydrogel particles
        irreversible_bond_object (`cls`): Irreversible bond object of the espresso library
        types (`dict`): Dictionary mapping the espresso types
        charges (`dict`): Dictionary mapping the charges of the espresso types
        system (`cls`): system object of the espresso library
        angle_object (`cls`): The angle object to apply a bending potential to the dangling ends. Defaults to None.
        
    Note: 
        Assumes a hydrogel network already exists, with a hydrogel map.
    
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU), 2024
    """
    from math import floor, ceil
    import numpy as np
    from lib import misc_tools as misc

    for fibril in find_fibrils(start_node_id=hydrogel_map["nodes"][0],end_node_id=hydrogel_map["nodes"][-1],monomers_per_fibril=monomers_per_fibril, hydrogel_map=hydrogel_map, system=system):
        fibril.pop(0) #first is startnode
        fibril.pop(-1) #last is endnode
        fibril = np.array(fibril)
        fibril_vector = system.part.by_id(fibril[-1]).pos - system.part.by_id(fibril[0]).pos

        bead_ids_to_add_ends_to = [] 
        divider = monomers_per_fibril // dangling_ends_per_fibril
        for part in range(dangling_ends_per_fibril):
            newList = fibril[divider*part:divider*(part+1)]
            if part > dangling_ends_per_fibril//2: #Hvis vi er over halvveis
                bead_ids_to_add_ends_to.append(newList[floor(len(newList)/2)])
            else:
                bead_ids_to_add_ends_to.append(newList[ceil(len(newList)/2)])

            

        for bead_id in bead_ids_to_add_ends_to:
            prev_particle = system.part.by_id(bead_id)
            perpendicular_vector = misc.generate_trial_perpendicular_vector(vector=fibril_vector, center=prev_particle.pos, radius=bond_length)
            dangling_end = []
            for dangling_bead in range(length_of_dangling_end):
                position = prev_particle.pos + perpendicular_vector
                p = system.part.add(pos=position, type=types["A"], q=charges[types["A"]])
                dangling_end.append(p.id)
                p.add_bond((irreversible_bond_object, prev_particle))
                prev_particle = p
                
            if angle_object:
                left_particle = system.part.by_id(bead_id)
                for i in range(0,len(dangling_end)-1):
                    middle_particle = system.part.by_id(dangling_end[i])
                    right_particle = system.part.by_id(dangling_end[i+1])
                    middle_particle.add_bond((angle_object, left_particle, right_particle))
                    left_particle = middle_particle
                                 
    return

def add_bending_potential(angle_object, type_map, mpc, hydrogel_map, system):
    """
    Adding a harmonic bending potential defined by the angle_obejcts to each particle triplet in the hydrogel.
    First, it adds the potential to all particle triplets in the 16 chains of the diamond hydrogel structure, including where the nodes are first and last particle of the triplet. 
    Then it adds the potential to the particle triplets with the node being in the middle of the triplet.
    
    The angle object for the particle triplets is given by the angle_object.
    In the node, the angle object is added two times, between two different chains as if the node is where two chains crosses each other.
    
    Args:
        angle_object (`cls`): The angle object defining the bending potential of a triplet
        type_map (`dict`): Dictionary mapping the espresso types
        mpc (`int`): The number of monomers per chain
        hydrogel_map (`dict`): Dictionary with ids of nodes and boundary particles
        system(`cls`): system object of the espresso library 
        
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU), 2024
    """
    
    for n_chains in range(16): #The diamond lattice has 16 chains
        first_particle_chain = system.part.by_id(8+n_chains*mpc) 
        
        #For when node is particle left
        particle_left = system.part.by_id(find_bonded_node(hydrogel_map=hydrogel_map, particle_id=first_particle_chain.id, system=system))
        particle_middle = system.part.by_id(first_particle_chain.id)
        particle_right = system.part.by_id(particle_middle.id + 1)
        particle_middle.add_bond((angle_object, particle_left, particle_right))
        
        for particle_increment in range(mpc-2):
            particle_left = system.part.by_id(first_particle_chain.id + particle_increment) 
            particle_middle = system.part.by_id(particle_left.id + 1)
            particle_right = system.part.by_id(particle_middle.id + 1)
            particle_middle.add_bond((angle_object, particle_left, particle_right))
            
        #For when the node is particle_right
        particle_left = particle_middle
        particle_middle = particle_right
        particle_right = system.part.by_id(find_bonded_node(hydrogel_map=hydrogel_map, particle_id=particle_right.id, system=system))
        particle_middle.add_bond((angle_object, particle_left, particle_right))
        
    for node in system.part.select(type=type_map["gel-n-nodes"]):
        bonded_particles = search_bonded_particle_ids(particle_id=node.id, system=system)
        particle_left_1 = system.part.by_id(bonded_particles[0])
        particle_right_1 = system.part.by_id(bonded_particles[1])
        particle_left_2 = system.part.by_id(bonded_particles[2])
        particle_right_2 = system.part.by_id(bonded_particles[3]) #all nodes are bonded to four particles
        node.add_bond((angle_object, particle_left_1, particle_right_1))
        node.add_bond((angle_object, particle_left_2, particle_right_2))
    return

def get_eq_swelling(structurefile):
    """
    Gets the swelling equilibrium value for the agarose structure defined by structurefile

    Args:
        structurefile (`str`): The string specifying which structurefile findign the swelling eq for

    Raises:
        ValueError: If the swelling eq of this structure does not exist

    Returns:
        `float`: The fres value for the agarose structure to be relaxed with
        
    Authors:
        - Charlotte Borge Hunskår, Norwegian University of Science and Technology (NTNU), 2024
    """
    
    from pmb_handy_tools.lib import analysis
    
    data_dict=analysis.get_params_from_dir_name(structurefile)

    fres = 1
    if data_dict["mpc"]=='30':
        if data_dict["dends"]=='0' and data_dict["ldends"]=='0':
            fres = 0.58
        elif data_dict["dends"]=='3' and data_dict["ldends"]=='15':
            fres = 0.7
            
    elif data_dict["mpc"]=='40':
        if data_dict["dends"]=='0' and data_dict["ldends"]=='0':
            fres = 0.54
            
    elif data_dict["mpc"]=='50':
        if data_dict["dends"]=='0' and data_dict["ldends"]=='0':
            fres = 0.52
            
    else:
        raise ValueError("The swelling eq of this structure does not exist")

    return fres